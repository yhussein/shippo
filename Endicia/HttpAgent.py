import urllib
from urllib2 import HTTPError
import requests
from API_Config import API

class HttpAgent:
    
    def __init__(self):
        self._url = None
        
    def _build_resource_url(self, method, xml):
        data = API.URL_GET_FORMAT_STR%(method, xml)
        self._url = API.BASE_URL + data

    def get_resource_url(self):
        return self._url
    
    def make_request(self, method, xml, dry_run=False):
        self._build_resource_url(method, xml)
#         if self._url is None:
#             raise Exception('Resource URL not set, use "build_resource_url"')
        
        try:
            if not dry_run:
                content = requests.get(self._url).content
                return content
            else:
                return None #usually this returns a mock response, if dry_run
            
        except HTTPError as e:
            print 'HttpError', e
            
        return None