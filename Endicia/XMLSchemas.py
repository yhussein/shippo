import xml.etree.ElementTree as ET

class XMLSchemas:
    def schema(type, account_id, pass_phrase):
        if type == 'getTransactionsListing': 
            return GetTransactionsListing(account_id, pass_phrase) 
        
    schema = staticmethod(schema)
    
    
class _xml_base:
    """ Base class with common methods for handling XML objects """
    
    def __init__(self, account_id, user_pass):
        self._account_id = account_id
        self._user_pass = user_pass
        
    def _parse_raw_xml(self, xml_raw):
        return ET.fromstring(xml_raw.strip().rstrip())
    
    def parse_response(self, res, child_tag_path=None):
        """ Return a list of elements if resource_locator is set.
            Return the entire tree otherwise
        """
        parsed_xml = self._parse_raw_xml(res)
        
        if child_tag_path is None:
            return parsed_xml
        
        return parsed_xml.find(child_tag_path)
    
class GetTransactionsListing(_xml_base):
    def _xml_template(self):
        return """<?xml version="1.0" encoding="UTF-8"?> 
                        <TransactionsListingRequest>
                            <AccountID>%s</AccountID>
                            <PassPhrase>%s</PassPhrase>
                            <Test>%s</Test>
                            <TransactionType>%s</TransactionType>
                            <StartDate>%s</StartDate>
                            <StartTime>%s</StartTime>
                            <EndDate>%s</EndDate>
                            <EndTime>%s</EndTime>
                            <Results>%s</Results>
                            <ToZipCode>%s</ToZipCode>
                            <FromAddress>%s</FromAddress>
                            <RefernceId>%s</RefernceId>
                            <CostCenter>%s</CostCenter>
                            <TrackingList> 
                                <PICNumber>%s</PICNumber>
                                <PieceID>%s</PieceID>
                                <CustomsID>%s</CustomsID>
                            </TrackingList>
                          </TransactionsListingRequest>"""
                          
                          
    def build_xml(self, test='N',
                        transaction_type='',
                        from_address='',
                        start_date='',
                        start_time='',
                        end_date='',
                        end_time='',
                        to_zip_code='',
                        results='',
                        pic_number='',
                        customs_id='',
                        piece_id='',
                        reference_id='',
                        cost_center=''):
        
        template = self._xml_template()%(self._account_id,
                                          self._user_pass,
                                          test,
                                          transaction_type,
                                          start_date,
                                          start_time,
                                          end_date,
                                          end_time,
                                          results,
                                          to_zip_code,
                                          from_address,
                                          reference_id,
                                          cost_center,
                                          pic_number,
                                          piece_id,
                                          customs_id)
        return template.strip().rstrip()