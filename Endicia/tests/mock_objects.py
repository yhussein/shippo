import random
from datetime import date
import time
import string
from pyparsing import alphanums

class rand_mocks:
    def random_in_range(self, size, alphanum=False):
        """ Return a random int of some size.
            
            Args
                size: size of random int
                alphanum: return rand str from a alphanum set
        """
        
        size = size-1
        start = int('1'+('0'*size))
        end = int('9'+('9'*size))
        
        rand = ''
        if alphanum:
            rand = (''.join(random.choice(string.ascii_uppercase) for i in range(size+1)))
        else:
            rand = random.randint(start, end)
            
        return rand
    
    def boolean_rand(self, str=True):
        """ Return a random boolean value.
            
            Args
                str: return str formated boolean value if true, built-in boolean type otherwise. 
        """
        
        t = 'Y'; f = 'N'
        if not str:
            t = True; f = False;
            
        r = self.random_in_range(2)
        if (r%2)==0:
            return t
        return f
    
    def random_date(self):
        """ Return a random date pair in five day range from current date """

        five_days_delta = (24*60*60)*5 #24hrs * 60min *60secs
        timestamp = int(time.time())
        date1 = random.randint(timestamp-five_days_delta, 
                               timestamp)
        
        return [ str(date.fromtimestamp(date1)), 
                 str(date.fromtimestamp(timestamp)) ]
    
    def random_time(self):
        """ Return random time pair within five hours from current time"""
        pass
    
class mock_objects:
    
    def transactions_listing_rand(self):
        """ Generate and return a mock object with random values.
            Values type, size and other info taken form Endicia docs, p.103/sec.14
        """
        
        r = rand_mocks()
        s_date, e_date = r.random_date()
        mock_values = dict(account_id=str(r.random_in_range(7)),
                    pass_phrase=str(r.random_in_range(64, True)),
                    test='Y',
                    from_address=str(r.boolean_rand()),
                    start_date=s_date,
                    end_date=e_date,
                    start_time='',
                    end_time='',
                    to_zip_code=str(r.random_in_range(5)),
                    pic_number=str(r.random_in_range(31)),
                    customs_id=r.random_in_range(25, alphanum=True),
                    piece_id=r.random_in_range(10))
        
        xml_mock =      """<?xml version="1.0" encoding="UTF-8"?> 
                        <TransactionsListingRequest>
                            <AccountID>%s</AccountID>
                            <PassPhrase>%s</PassPhrase>
                            <Test>%s</Test>
                            <TransactionType>%s</TransactionType>
                            <StartDate>%s</StartDate>
                            <StartTime>%s</StartTime>
                            <EndDate>%s</EndDate>
                            <EndTime>%s</EndTime>
                            <Results>%s</Results>
                            <ToZipCode>%s</ToZipCode>
                            <FromAddress>%s</FromAddress>
                            <RefernceId>%s</RefernceId>
                            <CostCenter>%s</CostCenter>
                            <TrackingList> 
                                <PICNumber>%s</PICNumber>
                                <PieceID>%s</PieceID>
                                <CustomsID>%s</CustomsID>
                            </TrackingList>
                          </TransactionsListingRequest>"""%(mock_values['account_id'],
                                                           mock_values['pass_phrase'],
                                                           mock_values['test'],
                                                           '',
                                                           mock_values['start_date'],
                                                           '',
                                                           mock_values['end_date'],
                                                           '',
                                                           '',
                                                           mock_values['to_zip_code'],
                                                           mock_values['from_address'],
                                                           '',
                                                           '',
                                                           mock_values['pic_number'],
                                                           mock_values['piece_id'],
                                                           mock_values['customs_id'])
                          
        return {'mock_obj': xml_mock.strip().rstrip(), 'mock_values': mock_values}

    
    