import unittest
from mock_objects import mock_objects
from Endicia.XMLSchemas import XMLSchemas
import Endicia.HttpAgent as HA 
from Endicia.API_Config import API
from Endicia import Endicia

class tests(unittest.TestCase):
    
    def test_build_xml(self):
        #generate mock object with random mock data
        
        mock = mock_objects().transactions_listing_rand()
        mock_xml = mock['mock_obj']
        mock_values = mock['mock_values']
        
        #build xml obj using rand mock values
        schema = XMLSchemas.schema('getTransactionsListing', 
                                   mock_values['account_id'], 
                                   mock_values['pass_phrase'])

        xml_obj = schema.build_xml(test=mock_values['test'], 
                                     transaction_type='', 
                                     from_address=mock_values['from_address'], 
                                     start_date=mock_values['start_date'], 
                                     end_date=mock_values['end_date'], 
                                     to_zip_code=mock_values['to_zip_code'], 
                                     pic_number=mock_values['pic_number'], 
                                     customs_id=mock_values['customs_id'], 
                                     piece_id=mock_values['piece_id'])
        
        #compare both xml objects
        self.assertTrue(xml_obj == mock_xml, 'xml obj does not match mock obj')
        
        
    def test_resource_urlt(self):
        base_url = 'https://www.endicia.com/ELS/ELSServices.cfc?wsdl'
        get_params = '&method=%s&XMLInput=%s'
        
        #test for exact url
        self.assertTrue(API.BASE_URL == base_url, 'base url has changed or is invalid')
        self.assertTrue(API.URL_GET_FORMAT_STR == get_params, 'format str for url GET params is invalid')
        
        get_values = get_params%('methodName', 'xml_input')
        url = base_url + get_values
        
        #build url using HttpAgent
        ha = HA.HttpAgent()
        ha.make_request('methodName', 'xml_input', dry_run=True)
        
        self.assertTrue(url == ha.get_resource_url(), 'resource url created by HttpAgent is invalid')
        
    def test_http_agent_request(self):
        mock = mock_objects()
        mock_obj = mock.transactions_listing_rand()
        mock_xml = mock_obj['mock_obj']
        
        
        #build http request
        ha = HA.HttpAgent()
#         ha.build_resource_url('GetTransactionsListing', mock_xml)
        res = ha.make_request('GetTransactionsListing', mock_xml)    
        self.assertTrue('TransactionsListingResponse' in res, 'Invalid response by remote server. ==>'+res)
        
    def test_endicia(self):
        mock = mock_objects()
        mock_obj = mock.transactions_listing_rand()
        mock_values = mock_obj['mock_values']
        
        e = Endicia.Endicia(mock_values['account_id'], mock_values['pass_phrase'])
        xml_handler = e.getTramsactionsListing()
        xml_obj = xml_handler.build_xml(test=mock_values['test'], 
                                      from_address=mock_values['from_address'], 
                                      start_date=mock_values['start_date'], 
                                      end_date=mock_values['end_date'],
                                      to_zip_code=mock_values['to_zip_code'], 
                                      pic_number=mock_values['pic_number'], 
                                      customs_id=mock_values['customs_id'], 
                                      piece_id=mock_values['piece_id'])
        
        res = e.request(xml_obj)

        self.assertTrue('TransactionsListingResponse' in res, 'Endicia error')
