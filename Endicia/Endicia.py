from XMLSchemas import XMLSchemas
import HttpAgent as HA

class Endicia:
    def __init__(self, account_id, pass_phrase):
        #init main vals
        self._account_id = account_id
        self._pass_phrase = pass_phrase
        self._method = None
        
    def getTramsactionsListing(self):
        #set method name
        self._method = 'GetTransactionsListing'
        return XMLSchemas.schema('getTransactionsListing',
                                 self._account_id,
                                 self._pass_phrase)
        
    def request(self, xml_obj):
        if xml_obj is None or self._method is None:
            raise Exception('XML object must be built before making requests')
        
        #build request resources
        ha = HA.HttpAgent()
        res = ha.make_request(self._method, xml_obj)
        
        return res