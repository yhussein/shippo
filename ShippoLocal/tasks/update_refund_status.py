from Endicia.Endicia import Endicia
from ShippoLocal.models import Transactions

class UpdateRefundStatus:
    def run(self):
        """ Run task to check for transactions to be refunded. 
        Update refund status based on response from Endicia server
        """ 
        
        transactions = self.get_transactions_db()
        
        if transactions is None:
            return
        
        for transaction in transactions:
            ###assume TRANSACTION table has a foreign key to a USER table which contains account_id and pass_phrase###
            user = transaction.user
            code = self.make_request(user.account_id, 
                                     user.pass_phrase, 
                                     transaction.pic_number)
            
            if code is not None:
                transaction.refund_status = code
                transaction.save()
        
    def make_request(self, account_id, pass_phrase, pic_number):
        e = Endicia(account_id, pass_phrase)
        xml_handler = e.getTramsactionsListing()
        xml_obj = xml_handler.build_xml(test='Y', pic_number=pic_number)
        
        #make request
        res = e.request(xml_obj)
        #prse res using resource locator
        locator = 'TransactionResults/Transaction/RefundStatus'
        
        rc = xml_handler.parse_response(res, locator)
        
        if rc is not None:
            return rc.text
        
        return None
    
    def get_transactions_db(self):
        return None #return: Transactions.objects.filter(refund_order=True), if not testing on mock model
    
if __name__ == '__main__':
    UpdateRefundStatus().run()