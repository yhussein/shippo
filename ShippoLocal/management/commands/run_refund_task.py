from django.core.management.base import NoArgsCommand
from ShippoLocal.tasks.update_refund_status import UpdateRefundStatus

class Command(NoArgsCommand):
    help = 'run task to update refund status in transactions table'
    
    def handle_noargs(self, **args):
        #you can add this to crontab to run once a day
        #crontab -l | { cat; echo "0 0 * * * /dirc/to/this/file/manage.py run_refund_task"; } | crontab -
        print 'Running update refund status task...'
        #run task from tasks folder
        UpdateRefundStatus().run()