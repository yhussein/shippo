from django.db import models

""" Model is not complete. This is a simple mock model."""

class Transactions(models.Model):
    account_id = models.CharField(max_length=7)
    status = models.CharField(max_length=31)
    refund_status = models.CharField(max_length=1)
    pic_number = models.CharField(max_length=31)
    refund_order = models.BooleanField(default=False)